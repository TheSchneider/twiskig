package twisk;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import twisk.mondeIG.MondeIG;
import twisk.vues.VueMenu;
import twisk.vues.VueMondeIG;
import twisk.vues.VueOutils;

public class MainTwisk extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        MondeIG monde = new MondeIG(); // Le modèle

        BorderPane root = new BorderPane(); // La scène principale
        VueMenu vueMenu = new VueMenu(monde); // Le menu
        root.setTop(vueMenu);
        VueOutils vueOutils = new VueOutils(monde); // Les boutons de contrôle
        root.setBottom(vueOutils);
        VueMondeIG vueMondeIG = new VueMondeIG(monde); // La présentation du monde
        root.setCenter(vueMondeIG);

        root.getStylesheets().add("styles/generalStyle.css");
        // Affichage de la scène principale :
        primaryStage.setTitle("Interface Graphique Twisk");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
