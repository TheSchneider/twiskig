package twisk.exceptions;

import javafx.scene.control.Alert;
import twisk.vues.VueAlerte;

public class ExceptionArcSurLaMemeEtape extends TwiskException {
    public ExceptionArcSurLaMemeEtape() {
        super();
    }

    @Override
    public void afficherErreur() {
        VueAlerte alerte = new VueAlerte(Alert.AlertType.WARNING, "Un arc doit relier deux étapes différentes !");
    }
}
