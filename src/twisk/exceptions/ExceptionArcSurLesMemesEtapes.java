package twisk.exceptions;

import javafx.scene.control.Alert;
import twisk.vues.VueAlerte;

public class ExceptionArcSurLesMemesEtapes extends TwiskException {

    public ExceptionArcSurLesMemesEtapes() {
        super();
    }

    public void afficherErreur() {
        VueAlerte alerte = new VueAlerte(Alert.AlertType.WARNING, "Il ne peut y avoir qu'un seul arc reliant deux mêmes étapes !");
    }
}
