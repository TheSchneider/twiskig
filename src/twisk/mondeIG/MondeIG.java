package twisk.mondeIG;

import twisk.exceptions.ExceptionArcSurLaMemeEtape;
import twisk.exceptions.ExceptionArcSurLesMemesEtapes;
import twisk.exceptions.TwiskException;
import twisk.outils.FabriqueIdentifiant;
import twisk.outils.TailleComposants;
import twisk.vues.Observateur;
import twisk.vues.VueActiviteIG;
import twisk.vues.VueArcIG;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class MondeIG extends Sujet implements Iterable<EtapeIG>{

    HashMap<String, EtapeIG> etapes;
    ArrayList<EtapeIG> etapesSelectionnees;
    ArrayList<ArcIG> arcs;
    ArrayList<ArcIG> arcsSelectionnes;
    ArrayList<EtapeIG> entrees;
    ArrayList<EtapeIG> sorties;
    PointDeControleIG premierPoint;

    public MondeIG() {
        super();
        etapes = new HashMap<>();
        etapesSelectionnees = new ArrayList<>();
        arcsSelectionnes = new ArrayList<>();
        entrees = new ArrayList<>();
        sorties = new ArrayList<>();
        arcs = new ArrayList<>();
        premierPoint = null;
    }

    public boolean contientUneSeuleSelection() {
        return this.etapesSelectionnees.size() == 1;
    }

    public void ajouter(String type) {
        switch (type) {
            case "Activité" :
                int numero = etapes.size() + 1;
                // Nom : Activite x  (ou x est le nombre d'Etapes de l'Hashmap)
                String nomActivite = "Activite " + numero;
                String identifiantActivite = FabriqueIdentifiant.getInstance().getIdentifiantEtape();
                ActiviteIG activiteIG = new ActiviteIG(nomActivite, identifiantActivite, TailleComposants.getInstance().getlActivite(), TailleComposants.getInstance().gethActivite());
                relocate(activiteIG);
                etapes.put(identifiantActivite, activiteIG);
                VueActiviteIG vueActivite = new VueActiviteIG(this, activiteIG);
                break;
            case "Guichet" :
                System.out.println("l'ajout de Guichets n'est pas encore pris en charge");
                break;
            default :
                System.out.println("Erreur : type d'Etape inconnu");
                break;
        }
        notifierObservateurs();
    }

    /**
     * Permet d'ajouter un arc entre 2 points de contrôle en respectant certaines contraintes
     * @param pt1 le point de départ
     * @param pt2 le point d'arrivée
     */
    public void ajouter(PointDeControleIG pt1, PointDeControleIG pt2) throws TwiskException {
        // Contrainte 1 : les points de contrôle doivent être sur des étapes différentes :
        if (pt1.getEtape().equals(pt2.getEtape())) {
            throw new ExceptionArcSurLaMemeEtape();
        }
        // Contrainte 2 : il ne doit exister qu'un seul arc entre deux mêmes étapes
        Iterator<ArcIG> iteratorArcs = this.iteratorArcs();
        ArcIG arcAVerifier;
        while (iteratorArcs.hasNext()) {
            arcAVerifier = iteratorArcs.next();
            if (arcAVerifier.getDebut().getEtape().equals(pt1.getEtape()) && arcAVerifier.getFin().getEtape().equals(pt2.getEtape())
                    || arcAVerifier.getDebut().getEtape().equals(pt2.getEtape()) && arcAVerifier.getFin().getEtape().equals(pt1.getEtape())) {
                throw new ExceptionArcSurLesMemesEtapes();
            }
        }
        ArcIG arcAajouter = new ArcIG(pt1, pt2);
        this.arcs.add(arcAajouter);
        this.getObservateurs().add(new VueArcIG(this, arcAajouter));
        notifierObservateurs();
    }

    public void selectionnerEtape(EtapeIG etape) {
        if (estSelectionnee(etape)) {
            etapesSelectionnees.remove(etape);
        } else {
            etapesSelectionnees.add(etape);
        }
        this.notifierObservateurs();
    }

    public void selectionnerArc(ArcIG arc) {
        if (estSelectionne(arc)) {
            arcsSelectionnes.remove(arc);
        } else {
            arcsSelectionnes.add(arc);
        }
        this.notifierObservateurs();
    }

    public boolean estSelectionnee(EtapeIG etape) {return etapesSelectionnees.contains(etape);}
    public boolean estSelectionne(ArcIG arc) {return arcsSelectionnes.contains(arc);}

    public void selectionnerPoint(PointDeControleIG point) {
        if (premierPoint == null) {
            premierPoint = point;
        } else {
            try {
                ajouter(premierPoint, point);
            } catch (TwiskException te) {
                te.afficherErreur();
            }
            premierPoint = null;
        }
    }

    public void selectionEnEntree() {
        for (EtapeIG etape : etapesSelectionnees) {
            if (entrees.contains(etape)) {
                entrees.remove(etape);
            } else {
                entrees.add(etape);
                sorties.remove(etape);
            }
        }
        effacerLaSelection();
        notifierObservateurs();
    }

    public boolean estUneEntree(EtapeIG etape) {
        return entrees.contains(etape);
    }

    public void selectionEnSortie() {
        for (EtapeIG etape : etapesSelectionnees) {
            if (sorties.contains(etape)) {
                sorties.remove(etape);
            } else {
                sorties.add(etape);
                entrees.remove(etape);
            }
        }
        effacerLaSelection();
        notifierObservateurs();
    }

    public boolean estUneSortie(EtapeIG etape) {
        return sorties.contains(etape);
    }

    public void changerTempsSelection(int t) {
        EtapeIG etapeAChanger = etapesSelectionnees.get(0);
        etapeAChanger.setTemps(t);
        effacerLaSelection();
        notifierObservateurs();
    }

    public void changerEcartTempsSelection(int et) {
        EtapeIG etapeAChanger = etapesSelectionnees.get(0);
        etapeAChanger.setEcartTemps(et);
        effacerLaSelection();
        notifierObservateurs();
    }

    public void supprimerLaSelection() {
        ArrayList<Observateur> vuesASupprimer = new ArrayList<>();
        ArrayList<ArcIG> arcsASupprimer = new ArrayList<>();
        for (EtapeIG etape : etapesSelectionnees) {
            this.etapes.remove(etape.identifiant);
            // Suppression des vues associées (arcs et activités):
            for (Observateur obs : getObservateurs()) {
                if (obs instanceof VueActiviteIG) {
                    if (((VueActiviteIG) obs).getEtape().equals(etape)) {
                        vuesASupprimer.add(obs);
                    }
                }
                if (obs instanceof VueArcIG) {
                    if(((VueArcIG) obs).getArc().getDebut().getEtape().equals(etape) || ((VueArcIG) obs).getArc().getFin().getEtape().equals(etape)) {
                        vuesASupprimer.add(obs);
                        this.arcs.remove(((VueArcIG) obs).getArc());
                    }
                }
            }
        }
        for (ArcIG arc : arcsSelectionnes) {
            for (Observateur obs : getObservateurs()) {
                if (obs instanceof VueArcIG) {
                    if (((VueArcIG) obs).getArc().equals(arc)) {
                        vuesASupprimer.add(obs);
                        arcsASupprimer.add(arc);
                    }
                }
            }
        }
        arcs.removeAll(arcsASupprimer);
        getObservateurs().removeAll(vuesASupprimer);
        effacerLaSelection();
        notifierObservateurs();
    }

    public void renommerLaSelection(String nouveauNom) {
        String idEtapeARenommer = etapesSelectionnees.get(0).identifiant;
        etapes.get(idEtapeARenommer).setNom(nouveauNom);
        effacerLaSelection();
        this.notifierObservateurs();
    }

    public void effacerLaSelection() {
        etapesSelectionnees.clear();
        arcsSelectionnes.clear();
        notifierObservateurs();
    }

    public void deplacerEtape(String idEtape, double posX, double posY) {
        EtapeIG etapeADeplacer = etapes.get(idEtape);
        etapeADeplacer.setPosX((int) posX);
        etapeADeplacer.setPosY((int) posY);
        etapeADeplacer.resetPoints();
        notifierObservateurs();
    }

    @Override
    public Iterator<EtapeIG> iterator() {
        // On itère sur les étapes de la HashMap
        return etapes.values().iterator();
    }

    public Iterator<ArcIG> iteratorArcs() {
        // On itère sur les arcs de l'ArrayList
        return arcs.iterator();
    }

    /**
     * Change les coordonnées d'une étape pour ne pas qu'elles se chevauchent entre elles lors de leur création
     */
    public void relocate(EtapeIG etape) {
        boolean positionValide = false;
        int newPosX = 0, newPosY = 0; // Les nouvelles coordonnées de l'étape
        int posXOri, posYOri; // les coordonnées des étapes qu'on va comparer
        int l = TailleComposants.getInstance().getlActivite(); // largeur d'une etape
        int h = TailleComposants.getInstance().gethActivite(); // hauteur d'une etape
        Random randy = new Random();
        // Tant qu'on a pas trouvé de coordonnées valides :
        while (!positionValide) {
            // On génère de nouvelles coordonnées aléatoires :
            newPosX = randy.nextInt(TailleComposants.getInstance().getlMonde() - l);
            newPosY = randy.nextInt(TailleComposants.getInstance().gethMonde() - h);
            positionValide = true; // On suppose que les nouvelles coordonnées sont valides
            Iterator<EtapeIG> iterEtapes = this.iterator();
            while (iterEtapes.hasNext() && positionValide) {
                // On vérifie qu'il n y ait pas de collisions avec les autres Etapes
                EtapeIG aVerifier = iterEtapes.next();
                posXOri = aVerifier.getPosX();
                posYOri = aVerifier.getPosY();
                if (newPosX < posXOri + l && newPosX + l > posXOri && newPosY < posYOri + h && newPosY + h > posYOri) {
                    positionValide = false;
                }
            }
        }
        // On a trouvé des coordonnées valides :
        etape.setPosX(newPosX);
        etape.setPosY(newPosY);
        etape.resetPoints();
    }
}
