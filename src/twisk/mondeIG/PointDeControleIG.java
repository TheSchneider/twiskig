package twisk.mondeIG;

import twisk.outils.TailleComposants;

public class PointDeControleIG {

    // Position du point en son centre
    private int posX;
    private int posY;

    private String id; // identifiant unique
    private EtapeIG etape; // Etape à laquelle ce point est rattaché

    public PointDeControleIG(EtapeIG etapeLiee, char cote) {
        this.etape = etapeLiee;
        this.id = etapeLiee.getIdentifiant() + cote;
        switch (cote) {
            case 'h' :
                this.posX = etapeLiee.getPosX() + TailleComposants.getInstance().getlActivite()/2;
                this.posY = etapeLiee.getPosY();
                break;
            case 'd' :
                this.posX = etapeLiee.getPosX() + TailleComposants.getInstance().getlActivite();
                this.posY = etapeLiee.getPosY() + TailleComposants.getInstance().gethActivite()/2;
                break;
            case 'b' :
                this.posX = etapeLiee.getPosX() + TailleComposants.getInstance().getlActivite()/2;
                this.posY = etapeLiee.getPosY() + TailleComposants.getInstance().gethActivite();
                break;
            case 'g' :
                this.posX = etapeLiee.getPosX();
                this.posY = etapeLiee.getPosY() + TailleComposants.getInstance().gethActivite()/2;
                break;
            default :
                System.out.println("Erreur : direction invalide pour le point de contrôle.");
        }
    }

    public int getPosX() {return this.posX;}
    public int getPosY() {return this.posY;}
    public EtapeIG getEtape() {return this.etape;}

    public void setPosX(int posX) {this.posX = posX;}
    public void setPosY(int posY) {this.posY = posY;}

}
