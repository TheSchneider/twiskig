package twisk.mondeIG.mondeIGtests;

import twisk.mondeIG.EtapeIG;
import twisk.mondeIG.MondeIG;

class MondeIGTest {

    @org.junit.jupiter.api.Test
    void ajouter() {
        MondeIG monde = new MondeIG();
        monde.ajouter("Activité");
        monde.ajouter("Activité");
        for (EtapeIG e : monde) {
            System.out.println(e);
        }
    }

    @org.junit.jupiter.api.Test
    void iterator() {
        MondeIG monde = new MondeIG();
        monde.ajouter("Activité");
        monde.ajouter("Activité");
        monde.ajouter("Activité");

        for (EtapeIG etapeIG : monde) {
            System.out.println(etapeIG.toString());
        }
    }
}