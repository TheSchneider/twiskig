package twisk.mondeIG;

import twisk.outils.TailleComposants;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class EtapeIG implements Iterable<PointDeControleIG>{

    String nom;
    String identifiant;
    int posX;
    int posY;
    int largeur;
    int hauteur;
    int temps = 0;
    int ecartTemps = 0;
    ArrayList<PointDeControleIG> points;

    public EtapeIG(String nom, String idf, int larg, int haut) {
        this.nom = nom;
        this.identifiant = idf;
        this.largeur = TailleComposants.getInstance().getlActivite();
        this.hauteur = TailleComposants.getInstance().gethActivite();
        // Instancie les points de contôle haut, droit, bas et gauche
        points = new ArrayList<>();
        points.add(new PointDeControleIG(this, 'h'));
        points.add(new PointDeControleIG(this, 'd'));
        points.add(new PointDeControleIG(this, 'b'));
        points.add(new PointDeControleIG(this, 'g'));
    }

    public String getNom() {return this.nom;}
    public int getPosX() {return posX;}
    public int getPosY() {return posY;}
    public String getIdentifiant() {return this.identifiant;}
    public int getTemps() {return temps;}
    public int getEcartTemps() {return ecartTemps;}

    public void setNom(String nom) {this.nom = nom;}
    public void setPosX(int posX) { this.posX = posX; }
    public void setPosY(int posY) { this.posY = posY; }
    public void setTemps(int temps){this.temps = temps;}
    public void setEcartTemps(int ecartTemps) {this.ecartTemps = ecartTemps;}

    /**
     * Déplace les points de contrôle en fonction de la position de l'étape
     */
    public void resetPoints() {
        points.get(0).setPosX(this.getPosX() + TailleComposants.getInstance().getlActivite()/2);
        points.get(0).setPosY(this.getPosY());
        points.get(1).setPosX(this.getPosX() + TailleComposants.getInstance().getlActivite());
        points.get(1).setPosY(this.getPosY() + TailleComposants.getInstance().gethActivite()/2);
        points.get(2).setPosX(this.getPosX()  + TailleComposants.getInstance().getlActivite()/2);
        points.get(2).setPosY(this.getPosY() + TailleComposants.getInstance().gethActivite());
        points.get(3).setPosX(this.getPosX());
        points.get(3).setPosY(this.getPosY() + TailleComposants.getInstance().gethActivite()/2);
    }

    @Override
    public Iterator<PointDeControleIG> iterator() {
        return points.iterator();
    }

    public String toString() {
        return "Nom:" + nom + "\tID:" + identifiant + "\tl:" + largeur + "\th:" + hauteur;
    }
}
