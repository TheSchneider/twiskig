package twisk.mondeIG;

public class ArcIG {

    private PointDeControleIG debut;
    private PointDeControleIG fin;

    public ArcIG(PointDeControleIG debut, PointDeControleIG fin) {
        this.debut = debut;
        this.fin = fin;
    }

    public PointDeControleIG getDebut() {
        return this.debut;
    }
    public PointDeControleIG getFin() {
        return this.fin;
    }

    public String toString() {
        return "Arc entre " + getDebut().getEtape() + " et " +getFin().getEtape();
    }
}
