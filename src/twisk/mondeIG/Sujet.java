package twisk.mondeIG;

import twisk.vues.Observateur;

import java.util.ArrayList;

public class Sujet {

    private ArrayList<Observateur> observateurs;

    public Sujet() {
        this.observateurs = new ArrayList<>();
    }

    public ArrayList<Observateur> getObservateurs() {
        return this.observateurs;
    }

    public void ajouterObservateur(Observateur obs) {
        this.observateurs.add(obs);
    }

    public void notifierObservateurs() {
        for (Observateur obs : observateurs) {
            obs.reagir();
        }
    }
}
