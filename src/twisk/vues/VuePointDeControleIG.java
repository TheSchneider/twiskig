package twisk.vues;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import twisk.mondeIG.MondeIG;
import twisk.mondeIG.PointDeControleIG;
import twisk.outils.TailleComposants;

public class VuePointDeControleIG extends Circle implements Observateur {

    public VuePointDeControleIG(MondeIG monde, PointDeControleIG point) {
        super();

        // Dimensions + positionnement :
        this.setRadius(TailleComposants.getInstance().getRadius());
        this.setCenterX(point.getPosX());
        this.setCenterY(point.getPosY());

        // On click :
        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                monde.selectionnerPoint(point);
            }
        });

        // Du style :
        this.setId("PointDeControle");
    }

    @Override
    public void reagir() {}
}
