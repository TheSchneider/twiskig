package twisk.vues;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import twisk.mondeIG.ArcIG;
import twisk.mondeIG.MondeIG;
import twisk.outils.TailleComposants;

public class VueArcIG extends Pane implements Observateur {

    private MondeIG monde;
    private ArcIG arc;
    private Line ligne;
    private Polyline teteDeFleche;

    public VueArcIG(MondeIG monde, ArcIG arc) {
        super();
        this.arc = arc;
        this.monde = monde;

        monde.ajouterObservateur(this);

        // L'arc en lui-même :
        ligne = new Line(arc.getDebut().getPosX(), arc.getDebut().getPosY(), arc.getFin().getPosX(), arc.getFin().getPosY());
        ligne.setId("Line");
        // Le triangle en bout d'arc :
        teteDeFleche = new Polyline();
        Double[] coordFleche = calculerCoordonnees(arc.getDebut().getPosX(), arc.getDebut().getPosY(), arc.getFin().getPosX(), arc.getFin().getPosY());
        teteDeFleche.getPoints().addAll(coordFleche);
        teteDeFleche.setId("TeteFleche");
        // du contrôle :
        ligne.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                monde.selectionnerArc(arc);
            }
        });
        teteDeFleche.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                monde.selectionnerArc(arc);
            }
        });

        this.getChildren().addAll(ligne, teteDeFleche);
    }

    public Double[] calculerCoordonnees(int x1, int y1, int x2, int y2) {
        int longueurFleche = TailleComposants.getInstance().getlFleche();
        double angleFleche = TailleComposants.getInstance().getAngle();
        double angleArc = Math.atan2(y2-y1, x2-x1);
        double angleVers3 = angleArc + angleFleche;
        double angleVers4 = angleArc - angleFleche;
        double x3, y3, x4, y4; // Les coordonnées à déterminer
        double dx3 = Math.cos(angleVers3) * longueurFleche;
        double dy3 = Math.sin(angleVers3) * longueurFleche;
        x3 = x2 - dx3;
        y3 = y2 - dy3;
        double dx4 = Math.cos(angleVers4) * longueurFleche;
        double dy4 = Math.sin(angleVers4) * longueurFleche;
        x4 = x2 - dx4;
        y4 = y2 - dy4;
        Double[] res = new Double[]{(double)x2, (double)y2, x3, y3, x4, y4, (double) x2, (double) y2};
        return res;
    }

    public ArcIG getArc() {
        return this.arc;
    }

    @Override
    public void reagir() {
        this.getChildren().clear();
        // L'arc en lui-même :
        ligne = new Line(arc.getDebut().getPosX(), arc.getDebut().getPosY(), arc.getFin().getPosX(), arc.getFin().getPosY());
        if (monde.estSelectionne(arc)) {
            ligne.setId("LineSelectionnee");
        } else {
            ligne.setId("Line");
        }
        // Le triangle en bout d'arc :
        teteDeFleche = new Polyline();
        Double[] coordFleche = calculerCoordonnees(arc.getDebut().getPosX(), arc.getDebut().getPosY(), arc.getFin().getPosX(), arc.getFin().getPosY());
        teteDeFleche.getPoints().addAll(coordFleche);
        if (monde.estSelectionne(arc)) {
            teteDeFleche.setId("TeteFlecheSelectionnee");
        } else {
            teteDeFleche.setId("TeteFleche");
        }
        // du contrôle :
        ligne.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                monde.selectionnerArc(arc);
            }
        });
        teteDeFleche.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                monde.selectionnerArc(arc);
            }
        });
        this.getChildren().addAll(ligne, teteDeFleche);
    }
}
