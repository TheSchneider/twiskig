package twisk.vues;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import twisk.ecouteurs.EcouteurOnDragDetected;
import twisk.mondeIG.EtapeIG;
import twisk.mondeIG.MondeIG;
import twisk.outils.TailleComposants;

public class VueActiviteIG extends VueEtapeIG {

    private ImageView icone;
    private HBox teteEtape;

    public VueActiviteIG(MondeIG monde, EtapeIG etape) {
        super(monde, etape);

        // Icone + Label :
        teteEtape = new HBox();
        teteEtape.setAlignment(Pos.CENTER);

        // Label :
        String description = etape.getNom() + "/n" + etape.getTemps() + " +/- " + etape.getEcartTemps();
        titre = new Label(description);
        teteEtape.getChildren().add(titre);

        // Icone : (initialement, ni entrée ni sortie)
        icone = null;

        this.getChildren().add(teteEtape);

        // La zone destinée à contenir les clients :
        HBox zoneClients = new HBox();
        zoneClients.setMaxWidth(TailleComposants.getInstance().getlZoneClients());
        zoneClients.setPrefHeight(TailleComposants.getInstance().gethZoneClients());
        zoneClients.setId("ZoneClients");
        this.getChildren().add(zoneClients);

        // La taille de l'activité :
        this.setMinSize(TailleComposants.getInstance().getlActivite(), TailleComposants.getInstance().gethActivite());
        // La position de l'activité :
        this.setLayoutX(etape.getPosX());
        this.setLayoutY(etape.getPosY());

        // Du style :
        this.setId("VueActiviteIG");

        // Du contrôle :
        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                monde.selectionnerEtape(etape);
            }
        });

        // Drag'n'drop :
        this.setOnDragDetected(new EcouteurOnDragDetected(this));
    }

    @Override
    public void reagir() {
        teteEtape.getChildren().retainAll(titre);

        this.setLayoutX(etape.getPosX());
        this.setLayoutY(etape.getPosY());

        if (getMonde().estSelectionnee(getEtape())) {
            this.setId("VueActiviteIGSelectionnee");
        } else {
            this.setId("VueActiviteIG");
        }

        if (getMonde().estUneEntree(etape)) {
            icone = new ImageView(new Image("images/icon_entrance.png"));
            teteEtape.getChildren().add(icone);
        }
        if (getMonde().estUneSortie(etape)) {
            icone = new ImageView(new Image("images/icon_exit.png"));
            icone.setFitHeight(24);
            icone.setFitWidth(24);
            icone.setPreserveRatio(true);
            teteEtape.getChildren().add(icone);
        }

        this.titre.setText(etape.getNom() + "\n" + etape.getTemps() + " +/- " + etape.getEcartTemps());
    }
}
