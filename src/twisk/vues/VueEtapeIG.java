package twisk.vues;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import twisk.mondeIG.EtapeIG;
import twisk.mondeIG.MondeIG;


public abstract class VueEtapeIG extends VBox implements Observateur {

    protected MondeIG monde;
    protected EtapeIG etape;
    protected Label titre; // La description de l'activité

    public VueEtapeIG(MondeIG monde, EtapeIG etape) {
        super();
        this.monde = monde;
        this.etape = etape;

        this.monde.ajouterObservateur(this);
    }

    public EtapeIG getEtape() {
        return etape;
    }

    public MondeIG getMonde() {
        return monde;
    }
}
