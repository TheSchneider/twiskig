package twisk.vues;

import javafx.scene.layout.Pane;
import twisk.ecouteurs.EcouteurOnDragDropped;
import twisk.ecouteurs.EcouteurOnDragOver;
import twisk.mondeIG.*;
import twisk.outils.TailleComposants;

import java.util.Iterator;

public class VueMondeIG extends Pane implements Observateur {

    private MondeIG monde;

    public VueMondeIG(MondeIG monde) {
        super();

        // Liaison Vue/Modèle :
        this.monde = monde;
        monde.ajouterObservateur(this);

        // Affichage du monde :
        Iterator<ArcIG> iterArcs = monde.iteratorArcs();
        while(iterArcs.hasNext()) {
            VueArcIG vueArc = new VueArcIG(monde, iterArcs.next());
            this.getChildren().add(vueArc);
        }
        for (EtapeIG etape : monde) {
            if (etape instanceof ActiviteIG) {
                VueActiviteIG vueActivite = new VueActiviteIG(monde, etape);
                this.getChildren().add(vueActivite);
            }
            for (PointDeControleIG p : etape) {
                VuePointDeControleIG point = new VuePointDeControleIG(monde, p);
                this.getChildren().add(point);
            }
        }
        // Drag'n'drop :
        this.setOnDragOver(new EcouteurOnDragOver());
        this.setOnDragDropped(new EcouteurOnDragDropped(monde));

        // Taille du Monde :
        this.setPrefSize(TailleComposants.getInstance().getlMonde(), TailleComposants.getInstance().gethMonde());

        // Du style :
        this.setId("VueMondeIG");
    }

    @Override
    public void reagir() {
        // On redessine tout :
        this.getChildren().clear();
        // Les arcs :
        for (Observateur obs : monde.getObservateurs()) {
            if (obs instanceof VueArcIG && !this.getChildren().contains(obs)) {
                this.getChildren().add((VueArcIG) obs);
            }
        }
        for (EtapeIG etape : monde) {
            if (etape instanceof ActiviteIG) {
                for (Observateur obs : monde.getObservateurs()) {
                    if (obs instanceof VueActiviteIG && !this.getChildren().contains(obs)) {
                        this.getChildren().add((VueActiviteIG)obs);
                    }
                }
            }
            for (PointDeControleIG p : etape) {
                VuePointDeControleIG point = new VuePointDeControleIG(monde, p);
                this.getChildren().add(point);
            }
        }
    }

}
