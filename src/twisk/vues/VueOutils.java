package twisk.vues;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import twisk.mondeIG.MondeIG;
import twisk.outils.TailleComposants;

public class VueOutils extends TilePane implements Observateur {

    private MondeIG monde;

    public VueOutils(MondeIG monde) {
        super();

        // Liaison Vue/Modèle :
        this.monde = monde;
        monde.ajouterObservateur(this);

        // Bouton pour ajouter une activité :
        Image iconeAjouter = new Image("images/icon_add.png");
        ImageView vueIcone = new ImageView(iconeAjouter);
        Button ajouterUneActivite = new Button("", vueIcone);
        ajouterUneActivite.setTooltip(new Tooltip("Ajouter Une Activité"));
        ajouterUneActivite.setOnAction(actionEvent -> monde.ajouter("Activité"));
        ajouterUneActivite.setPrefSize(TailleComposants.getInstance().getlBoutonAjouter(), TailleComposants.getInstance().gethBoutonAjouter());

        this.getChildren().add(ajouterUneActivite);

        // Du style :
        this.setId("VueOutils");
    }

    @Override
    public void reagir() {}
}
