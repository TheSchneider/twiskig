package twisk.vues;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import twisk.ecouteurs.*;
import twisk.mondeIG.MondeIG;

public class VueMenu extends MenuBar implements Observateur {

    private MondeIG monde;
    private MenuItem renommerLaSelection;
    private MenuItem changerTemps;
    private MenuItem changerEcartTemps;

    public VueMenu(MondeIG monde) {
        super();

        this.monde = monde;
        this.monde.ajouterObservateur(this);

        // Fichier
        Menu fichier = new Menu("Fichier");
        MenuItem quitter = new MenuItem("Quitter");
        quitter.setOnAction(new EcouteurQuitter());
        fichier.getItems().addAll(quitter);

        // Edition
        Menu edition = new Menu("Edition");
        MenuItem supprimerLaSelection = new MenuItem("Supprimer la sélection");
        supprimerLaSelection.setOnAction(new EcouteurSupprimerLaSelection(monde));
        this.renommerLaSelection = new MenuItem("Renommer la sélection");
        renommerLaSelection.setOnAction(new EcouteurRenommerLaSelection(monde));
        MenuItem effacerLaSelection = new MenuItem("Effacer la sélection");
        effacerLaSelection.setOnAction(new EcouteurEffacerLaSelection(monde));
        edition.getItems().addAll(supprimerLaSelection, renommerLaSelection, effacerLaSelection);

        // Monde
        Menu menumonde = new Menu("Monde");
        MenuItem entree = new MenuItem("Entrée");
        entree.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                monde.selectionEnEntree();
            }
        });
        MenuItem sortie = new MenuItem("Sortie");
        sortie.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                monde.selectionEnSortie();
            }
        });
        menumonde.getItems().addAll(entree, sortie);

        // Paramètres :
        Menu parametres = new Menu("Parametres");
        changerTemps = new MenuItem("Choisir Temps");
        changerTemps.setOnAction(new EcouteurChangerTemps(monde));
        changerEcartTemps = new MenuItem("Choisir Ecart-Temps");
        changerEcartTemps.setOnAction(new EcouteurChangerEcartTemps(monde));
        parametres.getItems().addAll(changerTemps, changerEcartTemps);

        this.getMenus().addAll(fichier, edition, menumonde, parametres);

        // Du style :
        this.setId("VueMenu");
    }

    @Override
    public void reagir() {
        if (monde.contientUneSeuleSelection()) {
            renommerLaSelection.setDisable(false);
            changerTemps.setDisable(false);
            changerEcartTemps.setDisable(false);
        } else {
            renommerLaSelection.setDisable(true);
            changerTemps.setDisable(true);
            changerEcartTemps.setDisable(true);
        }
    }
}
