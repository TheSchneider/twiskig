package twisk.vues;

import javafx.scene.control.Alert;

public class VueAlerte extends Alert implements Observateur {

    public VueAlerte(AlertType alertType, String message) {
        super(alertType);
        this.setContentText(message);
        this.showAndWait();
    }

    @Override
    public void reagir() {
    }
}
