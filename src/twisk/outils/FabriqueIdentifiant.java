package twisk.outils;

public class FabriqueIdentifiant {

    int noEtape;

    private static FabriqueIdentifiant instance = new FabriqueIdentifiant();

    private FabriqueIdentifiant() {
        noEtape = 0;
    }

    public static FabriqueIdentifiant getInstance() {
        return instance;
    }

    public String getIdentifiantEtape() {
        noEtape++;
        return "Etape" + noEtape;
    }
}
