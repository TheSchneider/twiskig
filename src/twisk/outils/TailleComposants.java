package twisk.outils;

public class TailleComposants {

    // Constantes définissant la taille des composants graphiques
    // Activités :
    int lActivite = 160;
    int hActivite = 110;
    // Zone clients des activités :
    int lZoneClients = lActivite - 12;
    int hZoneClients = hActivite - 40;
    // Monde :
    int lMonde = 800;
    int hMonde = 500;
    // Bouton ajouterActivite :
    int lBoutonAjouter = 50;
    int hBoutonAjouter = 50;
    // Points de contrôle :
    int radius = 5;
    // Têtes de flèche :
    int lFleche = 20;
    double angle = 0.35; // angle en radians


    private static TailleComposants instance = new TailleComposants();
    private TailleComposants() {}
    public static TailleComposants getInstance() {return instance;}

    public int getlActivite() {return lActivite;}
    public int gethActivite() {return hActivite;}
    public int getlZoneClients() {return lZoneClients;}
    public int gethZoneClients() {return hZoneClients;}
    public int getlMonde() {return lMonde;}
    public int gethMonde() {return hMonde;}
    public int getlBoutonAjouter() {return lBoutonAjouter;}
    public int gethBoutonAjouter() {return hBoutonAjouter;}
    public int getRadius() {return radius;}
    public int getlFleche() {return lFleche;}
    public double getAngle() {return angle;}
}
