package twisk.ecouteurs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;
import twisk.mondeIG.MondeIG;

public class EcouteurChangerEcartTemps implements EventHandler<ActionEvent> {

    private MondeIG monde;

    public EcouteurChangerEcartTemps(MondeIG monde) {
        this.monde = monde;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        TextInputDialog tid = new TextInputDialog();
        tid.setTitle("Changer l'écart-temps d'une Activité");
        tid.setContentText("Quel est l'écart-temps de cette activité ?");
        tid.showAndWait();
        monde.changerEcartTempsSelection(Integer.parseInt(tid.getResult()));
    }
}
