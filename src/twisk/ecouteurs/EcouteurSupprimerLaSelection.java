package twisk.ecouteurs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import twisk.mondeIG.MondeIG;

public class EcouteurSupprimerLaSelection implements EventHandler<ActionEvent> {

    private MondeIG monde;

    public EcouteurSupprimerLaSelection(MondeIG monde) {
        super();
        this.monde = monde;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        monde.supprimerLaSelection();
    }
}
