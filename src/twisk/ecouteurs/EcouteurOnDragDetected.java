package twisk.ecouteurs;

import javafx.event.EventHandler;
import javafx.scene.input.*;
import twisk.vues.VueActiviteIG;

public class EcouteurOnDragDetected implements EventHandler<MouseEvent> {

    private VueActiviteIG vue;

    public EcouteurOnDragDetected(VueActiviteIG vue) {
        super();
        this.vue = vue;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        Dragboard dragboard = vue.startDragAndDrop(TransferMode.MOVE);
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(vue.getEtape().getIdentifiant());
        clipboardContent.putImage(vue.snapshot(null, null));
        dragboard.setContent(clipboardContent);
        mouseEvent.consume();
    }
}
