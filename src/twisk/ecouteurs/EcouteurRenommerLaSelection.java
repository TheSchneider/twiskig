package twisk.ecouteurs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;
import twisk.mondeIG.MondeIG;

public class EcouteurRenommerLaSelection implements EventHandler<ActionEvent> {

    private MondeIG monde;

    public EcouteurRenommerLaSelection(MondeIG monde) {
        super();
        this.monde = monde;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        TextInputDialog tid = new TextInputDialog();
        tid.setTitle("Renommer la sélection");
        tid.setContentText("Choisissez le nouveau nom de cette étape");
        tid.showAndWait();
        monde.renommerLaSelection(tid.getResult());
    }
}
