package twisk.ecouteurs;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import twisk.mondeIG.MondeIG;
import twisk.outils.TailleComposants;

public class EcouteurOnDragDropped implements EventHandler<DragEvent> {

    private MondeIG monde;

    public EcouteurOnDragDropped(MondeIG monde) {
        super();
        this.monde = monde;
    }

    @Override
    public void handle(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        String idEtape = dragboard.getString();
        double newPosX = dragEvent.getSceneX();
        double newPosY = dragEvent.getSceneY();
        monde.deplacerEtape(idEtape, newPosX - TailleComposants.getInstance().getlActivite()/2, newPosY - TailleComposants.getInstance().gethActivite()/2 - 30);
        dragEvent.setDropCompleted(true);
        dragEvent.consume();
    }
}
