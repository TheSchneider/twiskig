package twisk.ecouteurs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import twisk.mondeIG.MondeIG;

public class EcouteurEffacerLaSelection implements EventHandler<ActionEvent> {

    private MondeIG monde;

    public EcouteurEffacerLaSelection(MondeIG monde) {
        super();
        this.monde = monde;
    }
    @Override
    public void handle(ActionEvent actionEvent) {
        monde.effacerLaSelection();
    }
}
