package twisk.ecouteurs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;
import twisk.mondeIG.MondeIG;

public class EcouteurChangerTemps implements EventHandler<ActionEvent> {

    private MondeIG monde;

    public EcouteurChangerTemps(MondeIG monde) {
        this.monde = monde;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        TextInputDialog tid = new TextInputDialog();
        tid.setTitle("Changer le temps d'une Activité");
        tid.setContentText("Combien de secondes passent en moyenne les clients dans cette activité ?");
        tid.showAndWait();
        monde.changerTempsSelection(Integer.parseInt(tid.getResult()));
    }
}
